terraform {
  backend "s3" {
    bucket = "zblakac-dev"
    dynamodb_table = "terraform-zblakac-dev"
    key = "zblakac-dev.json"
    region = "us-east-1"
  }
  required_version = ">=1.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.52"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14.0"
    }
  }
}

provider "aws" {
  region = local.region
}

provider "kubernetes" {}
provider "kubectl" {
#  host                   = module.eks.endpoint
#  cluster_ca_certificate = module.eks.kubeconfig-certificate-authority-data
#  exec {
#    api_version = "client.authentication.k8s.io/v1beta1"
#    command     = "aws"
#    args        = ["eks", "get-token", "--cluster-name", local.cluster_name]
#  }
}
#  token =
#}
#provider "kubectl" {
#  host = module.eks.endpoint
#  cluster_ca_certificate = base64decode(module.eks.kubeconfig-certificate-authority-data)
#}
provider "helm" {
#  kubernetes {
#    host = module.eks.endpoint
#    cluster_ca_certificate = module.eks.kubeconfig-certificate-authority-data
#
#    exec {
#      api_version = "client.authentication.k8s.io/v1beta1"
#      command     = "aws"
#      args        = ["eks", "get-token", "--cluster-name", module.eks.cluster_id]
#    }
#  }
}