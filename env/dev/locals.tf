locals {
  region = "us-east-1"
  env = "dev"
  cluster_name = "eks_cluster_${local.env}"
  cluster_version = "1.29"
}