
module "vpc" {
  source = "./modules/vpc"
  cidr_block = var.cidr_block
  env = local.env
  public_subnets = var.public_subnets
  private_subnets = var.private_subnets
  private_subnets_cidr_blocks = var.private_subnets
  public_subnets_cidr_blocks = var.public_subnets
  eks_cluster_name = local.cluster_name
}

module "eks" {
  depends_on = [module.vpc]
  source = "./modules/eks"
  eks_cluster_name = local.cluster_name
  env = local.env
  private_subnets_ids = module.vpc.pubilc_subnets_ids
  public_subnets_ids = module.vpc.pubilc_subnets_ids
  eks_cluster_version = local.cluster_version
  max_size = "5"
  min_size = "1"
  desired_size = "3"
  aws_region = local.region
  instance_type = var.instance_type
  capacity_type = var.capacity_type
  eks_serviceaccount_name = var.eks_serviceaccount_name
  karpenter_serviceaccount_namespace = var.karpetner_eks_serviceaccount_name
  karpetner_eks_serviceaccount_name = var.karpetner_eks_serviceaccount_name
  serviceaccount_namespace = var.serviceaccount_namespace
  kubeconfig_filepath = var.kubeconfig_filepath
  gitlab_oidc_arn = var.gitlab_oidc_arn
}

module "eks_config" {
  source = "./modules/eks_configuration"
#  depends_on = [module.eks]
  host =module.eks.endpoint
  cluster_ca_certificate = module.eks.kubeconfig-certificate-authority-data
  cluster_id = module.eks.cluster_id
  env = local.env
  eks_serviceaccount_name = var.eks_serviceaccount_name
  serviceaccount_namespace = var.serviceaccount_namespace
  sa-role-arn = module.eks.eks_cluster_autoscaler_arn
  node_group_role_arn = module.eks.node_group_role_arn
}

module "prometheus" {
  source = "./modules/prometheus"
  host =module.eks.endpoint
  cluster_ca_certificate = module.eks.kubeconfig-certificate-authority-data
  cluster_id = module.eks.cluster_id
  env = local.env
}

#module "istio" {
#  source = "./modules/istio"
#  host =module.eks.endpoint
#  cluster_ca_certificate = module.eks.kubeconfig-certificate-authority-data
#  cluster_id = module.eks.cluster_id
#  env = local.env
#}
#
#module "flagger" {
#  source = "./modules/flagger"
#  host =module.eks.endpoint
#  cluster_ca_certificate = module.eks.kubeconfig-certificate-authority-data
#  cluster_id = module.eks.cluster_id
#  env = local.env
#}