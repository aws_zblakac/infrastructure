cidr_block = "10.10.0.0/16"
capacity_type = "SPOT"
instance_type = "t3.medium"
serviceaccount_namespace = "kube-system"
eks_serviceaccount_name = "eks-service-account"
karpenter_serviceaccount_namespace = "karpenter"
karpetner_eks_serviceaccount_name = "karpenter_sa"
kubeconfig_filepath = "~/.kube/config"
gitlab_oidc_arn = "arn:aws:iam::410040632229:role/gitlab_oidc/{{SESSION_NAME}}"
