### GENERAL ###

### VPC MODULE ###

variable "cidr_block" {
  type = string
}

variable "public_subnets" {
  type = map(object({
    cidr_block        = string
    availability_zone = string
  }))
  default = {
    public_subnet_1 = {
      cidr_block = "10.10.100.0/24"
      availability_zone = "us-east-1a"
    }
    public_subnet_2 = {
      cidr_block = "10.10.101.0/24"
      availability_zone = "us-east-1b"
    }
  }
}

variable "private_subnets" {
  type = map(object({
    cidr_block        = string
    availability_zone = string
  }))
  default = {
    private_subnet_1 = {
      cidr_block = "10.10.10.0/24"
      availability_zone = "us-east-1a"
    }
    private_subnet_2 = {
      cidr_block = "10.10.11.0/24"
      availability_zone = "us-east-1b"
    }
  }
}

### EKS MODULE ###

variable "instance_type" {
  type = string
}

variable "kubeconfig_filepath" {
  type = string
}

variable "capacity_type" {
  type = string
}

variable "serviceaccount_namespace" {
  type = string
}

variable "eks_serviceaccount_name" {
  type = string
}

variable "karpenter_serviceaccount_namespace" {
  type = string
}

variable "karpetner_eks_serviceaccount_name" {
  type = string
}

variable "gitlab_oidc_arn" {
  type = string
}