terraform {
  required_version = ">=1.0.0"

  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14.0"
    }
    helm = {
      source = "hashicorp/helm"
    }
  }
}

provider "kubectl" {
  host = var.host
  cluster_ca_certificate = var.cluster_ca_certificate

  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    command     = "aws"
    args = ["eks", "get-token", "--cluster-name", var.cluster_id]
  }
}

provider "helm" {
  kubernetes {
    host = var.host
    cluster_ca_certificate = var.cluster_ca_certificate

    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "aws"
      args        = ["eks", "get-token", "--cluster-name", var.cluster_id]
    }
  }
}
