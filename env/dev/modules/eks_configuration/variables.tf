variable "env" {
  type = string
}

variable "sa-role-arn" {
  type = string
}

variable "host" {}

variable "cluster_ca_certificate" {}

variable "cluster_id" {}

variable "node_group_role_arn" {}

variable "eks_serviceaccount_name" {
  type = string
}

variable "serviceaccount_namespace" {
  type = string
}