data "kubectl_path_documents" "test" {
  pattern = "./manifests/eks_configuration/*.yaml"
  vars = {
    env = var.env
    node_group_role = var.node_group_role_arn
    sa-role-arn = var.sa-role-arn
    eks_serviceaccount_name = var.eks_serviceaccount_name
    serviceaccount_namespace = var.serviceaccount_namespace
  }
}

resource "kubectl_manifest" "config" {
  for_each = toset(data.kubectl_path_documents.test.documents)
  yaml_body = each.value
}