resource "aws_eks_cluster" "cluster" {
  name                      = var.eks_cluster_name
  role_arn                  = aws_iam_role.eks_role.arn
  enabled_cluster_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  vpc_config {
    subnet_ids = concat(var.private_subnets_ids)
  }

#  access_config {
#    authentication_mode                         = "API_AND_CONFIG_MAP"
#    bootstrap_cluster_creator_admin_permissions = true
#  }
  #  access_config {
  #    authentication_mode                         = "CONFIG_MAP"
  #    bootstrap_cluster_creator_admin_permissions = true
  #  }

  depends_on = [
    aws_iam_role_policy_attachment.attach_AmazonEKSVPCResourceController,
    aws_iam_role_policy_attachment.attach_AmazonEKSClusterPolicy
  ]

  tags = {
    Name = var.eks_cluster_name
  }
}


resource "aws_eks_node_group" "eks_node_group" {
  cluster_name  = var.eks_cluster_name
  node_group_name = "eks_node_group_${var.env}"
  capacity_type = var.capacity_type
  instance_types = [var.instance_type]
  force_update_version = true
  node_role_arn = aws_iam_role.eks_node_group_role.arn
  subnet_ids = concat(var.private_subnets_ids)
  version = var.eks_cluster_version
  release_version = nonsensitive(data.aws_ssm_parameter.eks_ami_release_version.value)

  scaling_config {
    desired_size = var.desired_size
    max_size     = var.max_size
    min_size     = var.min_size
  }

  update_config {
    max_unavailable = 1
  }

  depends_on = [
  aws_iam_role_policy_attachment.attach_AmazonEC2ContainerRegistryReadOnly,
  aws_iam_role_policy_attachment.attach_AmazonEKS_CNI_Policy,
  aws_iam_role_policy_attachment.attach_AmazonEKSWorkerNodePolicy]

  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }
}

#resource "aws_eks_access_entry" "oidc" {
#  cluster_name      = aws_eks_cluster.cluster.name
#  principal_arn     = var.gitlab_oidc_arn
#  type              = "STANDARD"
#}