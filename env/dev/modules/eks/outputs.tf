output "endpoint" {
  value = aws_eks_cluster.cluster.endpoint
}

#output "token" {
#  value = aws_eks_cluster.cluster.
#}

output "kubeconfig-certificate-authority-data" {
  value = base64decode(aws_eks_cluster.cluster.certificate_authority[0].data)
}

output "cluster_id" {
  value = aws_eks_cluster.cluster.id
}

output "node_group_role_arn" {
  value = aws_iam_role.eks_node_group_role.arn
}