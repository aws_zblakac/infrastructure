variable "env" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "eks_cluster_name" {
  type = string
}

variable "private_subnets_ids" {}

variable "public_subnets_ids" {}

variable "eks_cluster_version" {
  type = string
}

variable "capacity_type" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "min_size" {
  type = string
}

variable "desired_size" {
  type = string
}

variable "max_size" {
  type = string
}

variable "serviceaccount_namespace" {
  type = string
}

variable "eks_serviceaccount_name" {
  type = string
}

variable "karpenter_serviceaccount_namespace" {
  type = string
}

variable "karpetner_eks_serviceaccount_name" {
  type = string
}

variable "kubeconfig_filepath" {
  type = string
}

variable "gitlab_oidc_arn" {
  type = string
}

#variable "kubeconfig_filepath" {
#  type = string
#}

#variable "serviceaccount_namespace" {
#  type = string
#}