output "private_subnets_ids" {
  value = values(aws_subnet.private_subnets)[*].id
}

output "pubilc_subnets_ids" {
  value = values(aws_subnet.public_subnets)[*].id
}