resource "aws_eip" "elastic_ips" {
  for_each = aws_subnet.public_subnets
  vpc = true

  tags = {
    Name = "EIP-${each.key}-${var.env}"
  }
}

resource "aws_nat_gateway" "nat_gateways" {
  for_each = aws_subnet.public_subnets
  subnet_id = aws_subnet.public_subnets[each.key].id
  allocation_id = aws_eip.elastic_ips[each.key].id

  tags = {
    Name = "NAT-GW-${each.key}-${var.env}"
  }
  depends_on = [aws_eip.elastic_ips]
}