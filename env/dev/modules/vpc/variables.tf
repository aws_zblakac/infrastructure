variable "env" {
  type = string
}

variable "cidr_block" {
  type = string
}

variable "public_subnets" {
  type = map(object({
    cidr_block        = string
    availability_zone = string
  }))
}

variable "private_subnets" {
  type = map(object({
    cidr_block        = string
    availability_zone = string
  }))
}

variable "private_subnets_cidr_blocks" {}

variable "public_subnets_cidr_blocks" {}

variable "eks_cluster_name" {
  type = string
}
