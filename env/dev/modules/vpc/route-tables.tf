locals {
   private_public_mapping = zipmap(keys(var.private_subnets_cidr_blocks), keys(var.public_subnets_cidr_blocks))
}

resource "aws_route_table" "public_route" {
  vpc_id = aws_vpc.this.id
  depends_on = [aws_internet_gateway.this]

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this.id
  }

  tags = {
      Name = "public-route-table-${var.env}"
    }
}

resource "aws_route_table_association" "attach_public_route_tables" {
  for_each = var.public_subnets
  depends_on = [aws_route_table.public_route]
  route_table_id = aws_route_table.public_route.id
  subnet_id = aws_subnet.public_subnets[each.key].id
}

resource "aws_route_table" "private_route" {
  for_each = aws_nat_gateway.nat_gateways
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gateways[each.key].id
  }

    tags = {
      Name = "private-route-table-${each.key}-${var.env}"
    }
}

resource "aws_route_table_association" "attach_private_route_tables" {
  for_each = aws_subnet.private_subnets
  depends_on = [aws_subnet.private_subnets]
  subnet_id = aws_subnet.private_subnets[each.key].id
  route_table_id = aws_route_table.private_route[local.private_public_mapping[each.key]].id
}